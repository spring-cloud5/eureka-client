package com.example.demo.dao;

import com.example.demo.model.Test;
import org.springframework.stereotype.Repository;

public interface TestDao {

    Test addTest(Test t);
}
